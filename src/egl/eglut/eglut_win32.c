/*
 * Copyright (C) 2022 Yonggang Luo.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Authors:
 *    Yonggang Luo <luoyonggang@gmail.com>
 */

#include <stdbool.h>
#include <stdio.h>

#include <windows.h>

#include "eglutint.h"

static bool finished = false;
static LRESULT CALLBACK
window_proc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
   LRESULT lRet = 1;
   switch (message)
   {
   case WM_CREATE:
      finished = true;
      break;
   case WM_PAINT:
   {
      if (wparam == 0)
      {
         break;
      }
      struct eglut_state *_eglut = (struct eglut_state *)wparam;
      struct eglut_window *win = _eglut->current;

      if (win->display_cb)
         win->display_cb();
      eglSwapBuffers(_eglut->dpy, win->surface);
      ValidateRect(win->native.u.window, NULL);
      break;
   }
   case WM_DESTROY:
   {
      PostQuitMessage(0);
      break;
   }
   default:
      lRet = DefWindowProcW(hwnd, message, wparam, lparam);
      break;
   }
   return lRet;
}

void _eglutNativeInitDisplay(void)
{
   _eglut->surface_type = EGL_WINDOW_BIT;
   _eglut->native_dpy = NULL;
}

void _eglutNativeFiniDisplay(void)
{
}

void _eglutNativeInitWindow(struct eglut_window *win, const char *title,
                            int x, int y, int w, int h)
{
   wchar_t class_name[] = L"eglut";
   wchar_t window_name[] = L"eglut";
   HWND hwnd;
   HINSTANCE hcurrentinst = NULL;
   WNDCLASSW window_class;

   memset(&window_class, 0, sizeof(window_class));
   window_class.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
   window_class.cbClsExtra = 0;
   window_class.cbWndExtra = 0;
   window_class.lpfnWndProc = window_proc;
   window_class.hInstance = hcurrentinst;
   window_class.hIcon = LoadIcon(NULL, IDI_APPLICATION);
   window_class.hCursor = LoadCursor(NULL, IDC_ARROW);
   window_class.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
   window_class.lpszMenuName = NULL;
   window_class.lpszClassName = class_name;
   if (!RegisterClassW(&window_class))
   {
      fprintf(stderr, "Failed to register window class\n");
      exit(1);
   }

   DWORD wStyle = WS_VISIBLE | WS_POPUP | WS_BORDER | WS_SYSMENU | WS_CAPTION;

   /* create window */
   hwnd = CreateWindowW(class_name, window_name,
                        wStyle,
                        0, 0, w, h,
                        NULL, NULL, hcurrentinst, NULL);

   ShowWindow(hwnd, SW_SHOWDEFAULT);

   win->native.u.window = hwnd;
   win->native.width = w;
   win->native.height = h;
}

void _eglutNativeFiniWindow(struct eglut_window *win)
{
   CloseWindow(win->native.u.window);
}

#if 0

static int
lookup_keysym(KeySym sym)
{
   int special;

   switch (sym) {
   case XK_F1:
      special = EGLUT_KEY_F1;
      break;
   case XK_F2:
      special = EGLUT_KEY_F2;
      break;
   case XK_F3:
      special = EGLUT_KEY_F3;
      break;
   case XK_F4:
      special = EGLUT_KEY_F4;
      break;
   case XK_F5:
      special = EGLUT_KEY_F5;
      break;
   case XK_F6:
      special = EGLUT_KEY_F6;
      break;
   case XK_F7:
      special = EGLUT_KEY_F7;
      break;
   case XK_F8:
      special = EGLUT_KEY_F8;
      break;
   case XK_F9:
      special = EGLUT_KEY_F9;
      break;
   case XK_F10:
      special = EGLUT_KEY_F10;
      break;
   case XK_F11:
      special = EGLUT_KEY_F11;
      break;
   case XK_F12:
      special = EGLUT_KEY_F12;
      break;
   case XK_KP_Left:
   case XK_Left:
      special = EGLUT_KEY_LEFT;
      break;
   case XK_KP_Up:
   case XK_Up:
      special = EGLUT_KEY_UP;
      break;
   case XK_KP_Right:
   case XK_Right:
      special = EGLUT_KEY_RIGHT;
      break;
   case XK_KP_Down:
   case XK_Down:
      special = EGLUT_KEY_DOWN;
      break;
   default:
      special = -1;
      break;
   }

   return special;
}

static void
next_event(struct eglut_window *win)
{
   int redraw = 0;
   XEvent event;

   if (!XPending(_eglut->native_dpy)) {
      /* there is an idle callback */
      if (_eglut->idle_cb) {
         _eglut->idle_cb();
         return;
      }

      /* the app requests re-display */
      if (_eglut->redisplay)
         return;
   }

   /* block for next event */
   XNextEvent(_eglut->native_dpy, &event);

   switch (event.type) {
   case Expose:
      redraw = 1;
      break;
   case ConfigureNotify:
      win->native.width = event.xconfigure.width;
      win->native.height = event.xconfigure.height;
      if (win->reshape_cb)
         win->reshape_cb(win->native.width, win->native.height);
      break;
   case KeyPress:
      {
         char buffer[1];
         KeySym sym;
         int r;

         r = XLookupString(&event.xkey,
               buffer, sizeof(buffer), &sym, NULL);
         if (r && win->keyboard_cb) {
            win->keyboard_cb(buffer[0]);
         }
         else if (!r && win->special_cb) {
            r = lookup_keysym(sym);
            if (r >= 0)
               win->special_cb(r);
         }
      }
      redraw = 1;
      break;
   default:
      ; /*no-op*/
   }

   _eglut->redisplay = redraw;
}
#endif

void _eglutNativeEventLoop(void)
{
   MSG msg;
   for (;;)
   {
      int gotMsg = ( PeekMessage ( &msg, NULL, 0, 0, PM_REMOVE ) != 0 );
      if (gotMsg)
      {
         if (msg.message == WM_QUIT || msg.message == WM_CLOSE)
         {
            printf("Exited");
            break;
         }
         TranslateMessage(&msg);
         if (msg.message == WM_PAINT && msg.hwnd == _eglut->current->native.u.window) {
            msg.wParam = (WPARAM)_eglut;
         }
         DispatchMessageW(&msg);
      }
      else
      {
         /* there is an idle callback */
         if (_eglut->idle_cb) {
            _eglut->idle_cb();
         }
         SendMessage(_eglut->current->native.u.window, WM_PAINT, (WPARAM)_eglut, 0);
      }
   }
}
